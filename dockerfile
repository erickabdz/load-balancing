FROM node
LABEL Ericka Bermúdez
WORKDIR /app
COPY . .
RUN npm install
ENV INSTANCE=1
ENV PORT=3000
EXPOSE ${PORT}
CMD INSTANCE=${INSTANCE} PORT=${PORT}} node bin/www
# add & to put anything on background