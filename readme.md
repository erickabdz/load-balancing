Código original: https://gitlab.com/luis190991/balancingapp 

readme original 

--------------------

# Proyecto de ejemplo loadBalance

Este ejemplo es ilustrativo y solo busca ser una herramienta para demostrar algunos puntos importantes en la implementación de arquitecturas de software.


# ¿Cómo funciona?

Para poder ejecutar este proyecto solo debemos relaizar los siguientes pasos:
1. Clonar el repositorio.
2. Entrar a la carpeta raíz del proyecto y ejecutar el proyecto inyectando la variable de entorno INSTANCE:

				INSTANCE=1 node bin/www
3. Como resultado renderizará el numero que hayamos pasado del a siguiente manera: "La instancia ejecutada es la numero 1".


-------------------

# ¿Cómo usarlo?
1. Ir a la carpeta raíz
2. ```(sudo) docker-compose up``` 
3. añadimos un ```-d``` para que el compose lo controle en background 
4. ```docker ps``` podremos ver todas nuestras imágenes ejecutándose
5. ```docker compose down``` para detenerlos 
6. ```docker ps``` ya no veremos los contenedores





